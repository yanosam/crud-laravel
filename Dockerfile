FROM php:7.3-apache

WORKDIR /var/www

RUN apt-get update && apt-get -y install \
    acl \
    cron \
    curl \
    libfreetype6-dev \
    libjpeg-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libwebp-dev \
    libmagickwand-dev --no-install-recommends \
    libmcrypt-dev \
    zlib1g-dev \
    && rm -rf /var/lib/apt/lists/*

RUN pecl install imagick

RUN docker-php-ext-enable imagick

RUN docker-php-ext-install pdo_mysql

RUN docker-php-ext-configure gd --with-gd \
    --with-webp-dir \
    --with-jpeg-dir \
    --with-png-dir \
    --with-zlib-dir \
    --with-freetype-dir 

RUN docker-php-ext-install gd

RUN a2enmod rewrite ssl

ARG APACHECONF

COPY $APACHECONF /etc/apache2/sites-available/000-default.conf

RUN sed -ri -e ':a;N;$!ba; s/AllowOverride None/AllowOverride All/3' /etc/apache2/apache2.conf

COPY ./src .

CMD ["apache2-foreground"]