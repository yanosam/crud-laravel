import Vue from 'vue'
import Router from 'vue-router'
import Cliente from './views/Cliente.vue'
import AddCliente from "@/components/cliente/add.vue";
import EditCliente from "@/components/cliente/edit.vue";
import ViewCliente from "@/components/cliente/view.vue";

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'clientes',
      component: Cliente,
      children: [
        {
          path: 'add',
          name: 'clientes_add',
          component: AddCliente,
        }, {
          path: 'edit/:id',
          name: 'clientes_edit',
          props: true,
          component: EditCliente,
        }, {
          path: '/',
          name: 'clientes_view',
          component: ViewCliente,
        }
      ]
    }
  ]
})
