<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $fillable = ['cliente_id', 'file', 'description'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'uploads';
}
