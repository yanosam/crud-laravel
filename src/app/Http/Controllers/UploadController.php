<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Upload;

class UploadController extends Controller
{
    public function index()
    {
        $uploads = Upload::orderBy('id', 'desc')->paginate(25);
        return response($uploads, 200);
    }
  
    public function create()
    {
        return view('uploads.create');
    }
  
    public function store(Request $request)
    {  
        $file = $request->file('file');
        if(!$file->isValid()) {
            return response()->json(['invalid_file_upload'], 400);
        }
        $path = public_path() . '/uploads/';
        $file->move($path, $file->getClientOriginalName());

        $upload = new Upload;
        $upload->cliente_id = $request->cliente_id;
        $upload->description = $request->description;
        $upload->file = $file->getClientOriginalName();
        $upload->save();
        return response($upload, 201);
    }
  
    public function show($id)
    {
        $upload = Upload::where('cliente_id', $id)->get();
        return response($upload, 201);
    }
  
    public function destroy($id)
    {
        $upload = Upload::findOrFail($id);
        $upload->delete();
        return response($upload, 204);
    }
}