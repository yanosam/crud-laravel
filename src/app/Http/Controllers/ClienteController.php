<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller
{
    public function main()
    {
        return \File::get(public_path() . '/index.html');
    }

    public function index()
    {
        $clientes = Cliente::orderBy('id', 'desc')->paginate(25);
        return response($clientes, 200);
    }
  
    public function create()
    {
        return view('clientes.create');
    }
  
    public function store(Request $request)
    {
        $cliente = new Cliente;
        $cliente->name = $request->name;
        $cliente->email = $request->email;
        $cliente->save();
        return response($cliente, 201);
    }
  
    public function show($id)
    {
        $cliente = Cliente::findOrFail($id);
        return response($cliente, 201);
    }
  
    public function edit($id)
    {
        $cliente = Cliente::findOrFail($id);
        return response($cliente, 201);
    }
  
    public function update(Request $request, $id)
    {
        $cliente = Cliente::findOrFail($id);
        $cliente->name = $request->name;
        $cliente->email = $request->email;
        $cliente->save();
        return response($cliente, 201);
    }
  
    public function destroy($id)
    {
        $cliente = Cliente::findOrFail($id);
        $cliente->delete();
        return response($cliente, 201);
    }
}
